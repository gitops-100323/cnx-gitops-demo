# lab2
## Kustomization
```
mkdir -p clusters/default/security
cat <<EOF > clusters/default/security/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- sync.yaml
EOF

cat <<EOF > clusters/default/security/sync.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: security-sync
  namespace: flux-system
spec:
  targetNamespace: app1
  interval: 1m0s
  path: ./security/sops-kms-yaml
  prune: true
  wait: true
  sourceRef:
    kind: GitRepository
    name: secretrepo
  decryption:
    provider: sops
EOF
```
## .sops
sops -e -i sops-kms-yaml/prod-secrets.yaml
echo -n '#111111' | base64
```
mkdir -p security/sops-kms-yaml
cat <<EOF > security/.sops.yaml
creation_rules:
  - path_regex: .*.yaml
    encrypted_regex: ^(data|stringData)$
    kms: arn:aws:kms:ap-southeast-1:290913681714:key/f322a072-9d03-4f2b-9b3f-67d65c58f191
EOF
cat <<EOF > security/sops-kms-yaml/prod-secrets.yaml
apiVersion: v1
data:
    podinfoUIColor: "IzExMTExMQ=="
kind: Secret
metadata:
    namespace: default
    creationTimestamp: null
    name: prod-secrets
EOF
```
## patch service account
```
patches:
      - patch: |
          apiVersion: v1
          kind: ServiceAccount
          metadata:
            name: kustomize-controller
            annotations:
              eks.amazonaws.com/role-arn: "arn:aws:iam::290913681714:role/eks-iam-serviceaccount-flux-gitops80-demo"
        target:
            kind: ServiceAccount
            name: kustomize-controller
```
note: restart kustomize-controller after patched
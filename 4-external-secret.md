# lab2
## Kustomization
```
mkdir -p clusters/default/platform/external-secrets
cat <<EOF > clusters/default/platform/external-secrets/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- sync.yaml
EOF

cat <<EOF > clusters/default/platform/external-secrets/sync.yaml
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: external-secrets
  namespace: flux-system
spec:
  interval: 1m0s
  path: ./platform/external-secrets
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
EOF
```
## External-secret Helm
```
mkdir -p platform/external-secrets
cat <<EOF > platform/external-secrets/external-secrets-helm.yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: external-secrets
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: HelmRepository
metadata:
  name: external-secrets
  namespace: flux-system
spec:
  interval: 1h0s
  url: https://charts.external-secrets.io
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: external-secrets
  namespace: flux-system
spec:
  chart:
    spec:
      chart: external-secrets
      sourceRef:
        kind: HelmRepository
        name: external-secrets
      version: '*'
  interval: 1h0s
  releaseName: external-secrets
  targetNamespace: external-secrets
  install:
    crds: Create
  upgrade:
    crds: CreateReplace
EOF
```
## secret-store
```
cat <<EOF > platform/external-secrets/secret-store.yaml
apiVersion: external-secrets.io/v1beta1
kind: ClusterSecretStore
metadata:
  name: example-store
spec:
  provider:
    aws:
      service: SecretsManager
      region: ap-southeast-1
      auth:
        secretRef:
          accessKeyIDSecretRef:
            name: awssm-secret
            namespace: default
            key: access-key
          secretAccessKeySecretRef:
            name: awssm-secret
            namespace: default
            key: secret-access-key
EOF
```
## secret-example
```
cat <<EOF > platform/external-secrets/prod-test-secret.yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: example
  namespace: external-secrets
spec:
  refreshInterval: 1m
  secretStoreRef:
    name: example-store
    kind: ClusterSecretStore
  target:
    name: prod-test-secret
    creationPolicy: Owner
  data:
  - secretKey: key1
    remoteRef:
      key: prod-test
      property: key1
EOF
```